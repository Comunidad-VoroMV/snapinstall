#!/bin/bash
#echo "Antes de Comenzar quiero dejar claro que este script es libre y no oficial. Esta basado en la documentación oficial del proyecto snap, para facilitar su instalación a los usuarios."
echo
echo Bienvenido al instalador de snap.
echo
echo  "Comprobaré que distribución usas para continuar:"
echo

#VERSION="grep '^(VERSION)=' /etc/os-release"
#NAME="grep '^(NAME)=' /etc/os-release"
#ID=grep '^(ID)=' /etc/os-release
NAME=$(egrep '^(NAME)=' /etc/os-release)
#echo El nombre es: $NAME
VERSION=$(egrep '^(VERSION)=' /etc/os-release)
#echo La versión es: $VERSION
ID=$(egrep '^(ID)=' /etc/os-release)

if [ "$ID" == "ID=arch" ]
then 
echo "Tu distribución es Arch Linux"
sleep 1s
echo
git clone https://aur.archlinux.org/snapd.git && cd snapd && makepkg -si && sudo systemctl enable --now snapd.socket && sudo ln -s /var/lib/snapd/snap /snap
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=debian" ]
then 
echo "Tu distribución es Debian"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd ; sudo snap install core
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=fedora" ]
then 
echo "Tu distribución es Fedora"
sleep 1s
echo
sudo dnf update
sudo dnf install -y snapd ; sudo ln -s /var/lib/snapd/snap /snap
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=kali" ]
then 
echo "Tu distribución es Kali Linux"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd ; systemctl enable --now snapd apparmor
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=kubuntu" ]
then 
echo "Tu distribución es Kubuntu"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=lubuntu" ]
then 
echo "Tu distribución es Lubuntu"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=opensuse-tumbleweed" ]
then 
echo "Tu distribución es openSUSE Tumbleweed"
sleep 1s
echo
sudo zypper ar https://download.opensuse.org/repositories/system:/snappy/openSUSE_Tumbleweed/ snappy && sudo zypper ar https://download.opensuse.org/repositories/system:/snappy/openSUSE_Leap_$releasever/ snappy && sudo zypper --gpg-auto-import-keys refresh && sudo zypper dup --from snappy && sudo zypper install snapd && sudo systemctl enable --now snapd && sudo systemctl enable --now snapd.apparmor
echo
    read -p "Quieres instalar un snap de prueba (s/n)?" sn
    case $sn in
        [Ss]* )  sudo snap install hello-world;;
        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=popos" ]
then 
echo "Tu distribución es Pop! _OS"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

#if [ "$ID" == "ID=RHEL" ]
#then 
#echo "Tu distribución es Red Hat Enterprise Linux (RHEL 8)"
#sleep 1s
#echo
#sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && sudo dnf upgrade && sudo yum install snapd && sudo systemctl enable --now snapd.socket && sudo ln -s /var/lib/snapd/snap /snap
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

#if [ "$ID" == "ID=solus" ]
#then 
#echo "Tu distribución es Solus"
#sleep 1s
#echo
#sudo eopkg up
#sudo eopkg install snapd
#echo
#echo "Es posible que necesites reiniciar el sistema para poder empezar a usar snap"
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

if [ "$ID" == "ID=xubuntu" ]
then 
echo "Tu distribución es Xubuntu"
sleep 1s
echo
sudo apt update
sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

#if [ "$ID" == "ID=centos" ]
#then 
#echo "Tu distribución es CentOs 8"
#sleep 1s
#echo
#sudo dnf install epel-release && sudo dnf upgrade && sudo yum install snapd && sudo systemctl enable --now snapd.socket && sudo ln -s /var/lib/snapd/snap /snap
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

if [ "$ID" == "ID=elementary" ]
then 
echo "Tu distribución es Elementary Os"
sleep 1s
echo
sudo apt update && sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

#if [ "$ID" == "ID=galliumos" ]
#then 
#echo "Tu distribución es GalliumOS"
#sleep 1s
#echo
#sudo apt update && sudo apt install -y snapd
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

if [ "$ID" == "ID=kdeneon" ]
then 
echo "Tu distribución es KDE Neón"
sleep 1s
echo
sudo apt update && sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=linuxmint" ]
then 
echo "Tu distribución es Linux Mint"
sleep 1s
echo
sudo mv /etc/apt/preferences.d/nosnap.pref ~/Documents/nosnap.backup && sudo apt update && sudo apt install -y snapd
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

if [ "$ID" == "ID=manjaro" ]
then 
echo "Tu distribución es Manjaro"
sleep 1s
echo
sudo pacman -S snapd && sudo systemctl enable --now snapd.socket && sudo ln -s /var/lib/snapd/snap /snap
echo
echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
fi

#if [ "$ID" == "ID=parrotos" ]
#then 
#echo "Tu distribución es Parrot Security OS"
#sleep 1s
#echo
#sudo apt update && sudo apt install -y snapd
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

#if [ "$ID" == "ID=raspberryos" ]
#then 
#echo "Tu distribución es Raspberry Pi OS"
#sleep 1s
#echo
#sudo apt update && sudo apt install -y snapd && sudo snap install core
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

#if [ "$ID" == "ID=rockylinux" ]
#then 
#echo "Tu distribución es Rocky Linux"
#sleep 1s
#echo
#sudo dnf install epel-release && sudo dnf upgrade ; sudo yum install snapd ; sudo systemctl enable --now snapd.socket ; sudo ln -s /var/lib/snapd/snap /snap
#echo
#echo "Ahora puedes instalar un paquete snap de prueba si quieres" ; sudo snap install hello-world
#fi

if [ "$ID" == "ID=ubuntu" ]
then 
echo "Tu distribución es Ubuntu"
sleep 1s
echo
echo "Si está ejecutando Ubuntu 16.04 LTS (Xenial Xerus) o posterior, no necesita hacer nada. Snap ya está instalado y listo para funcionar."
echo
#echo "Te mostraré tu versión de Ubuntu"
#echo
#lsb_release -a
fi

if [ "$ID" == "ID=zorinos" ]
then 
echo "Tu distribución es Zorin Os"
sleep 1s
echo
echo "Si está ejecutando Zorin 12.3 y superior, no necesita hacer nada. Snap ya está instalado y listo para funcionar."
echo
echo "Te mostraré tu versión de Zorin"
lsb_release -a
fi

echo "Ya has terminado, ya tienes snap instalado en tu sistema. A continuación debes reiniciar para aplicar los cambios."
echo
echo "La documentación oficial de instalación de snap puedes encontrarla en el siguiente enlace:https://snapcraft.io/docs/installing-snapd"
echo
echo "Gracias por usar este script"
sleep 2s ; echo